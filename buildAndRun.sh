#!/bin/sh
mvn clean package && docker build -t pl.lsea/togetheree .
docker rm -f togetheree || true && docker run -d -p 9080:9080 -p 9443:9443 --name togetheree pl.lsea/togetheree