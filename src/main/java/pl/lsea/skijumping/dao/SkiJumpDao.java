package pl.lsea.skijumping.dao;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import pl.lsea.skijumping.entity.SkiJump;

public class SkiJumpDao {
    
    EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("prod");
    EntityManager entityManager = entityManagerFactory.createEntityManager();
    
    public SkiJump findById(Long id) {
        return entityManager.find(SkiJump.class, id);
    } 
    
    public List<SkiJump> findAll() {
        entityManager.clear();
        
        return entityManager
                .createQuery("from SkiJump")
                .getResultList();
    }
    
    public SkiJump save(SkiJump skiJump) {
        entityManager.getTransaction().begin();
        
        if(skiJump.getId() != null) {
            entityManager.merge(skiJump);
        } else {
            entityManager.persist(skiJump);
        }
        
        entityManager.getTransaction().commit();
        
        return skiJump;
    }
    
    public void deleteById(Long id) {
        entityManager.getTransaction().begin();
        entityManager.remove(this.findById(id));
        entityManager.getTransaction().commit();
    }
}
