package pl.lsea.skijumping.dao;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import pl.lsea.skijumping.entity.SkiJumper;

public class SkiJumperDao {
    
    EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("prod");
    EntityManager entityManager = entityManagerFactory.createEntityManager();
    
    public SkiJumper findById(Long id) {
        return entityManager.find(SkiJumper.class, id);
    }
    
    public List<SkiJumper> findAll() {
        entityManager.clear();
        
        return entityManager
                .createQuery("from SkiJumper")
                .getResultList();
    }
    
    public SkiJumper save(SkiJumper skiJumper) {
        entityManager.getTransaction().begin();
        
        if(skiJumper.getId() != null) {
            entityManager.merge(skiJumper);
        } else {
            entityManager.persist(skiJumper);
        }
        
        entityManager.getTransaction().commit();
        
        return skiJumper;
    }
    
    public void deleteById(Long id) {
        entityManager.getTransaction().begin();
        entityManager.remove(this.findById(id));
        entityManager.getTransaction().commit();
    }
}
