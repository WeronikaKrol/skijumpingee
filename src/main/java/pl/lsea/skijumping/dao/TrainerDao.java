package pl.lsea.skijumping.dao;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import pl.lsea.skijumping.entity.Trainer;

public class TrainerDao {
    
    EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("prod");
    EntityManager entityManager = entityManagerFactory.createEntityManager();
    
    public Trainer findById(Long id) {
        return entityManager.find(Trainer.class, id);
    } 
    
    public List<Trainer> findAll() {
        entityManager.clear();
        
        return entityManager
                .createQuery("from Trainer")
                .getResultList();
    }
    
    public Trainer save(Trainer trainer) {
        entityManager.getTransaction().begin();
        
        if(trainer.getId() != null) {
            entityManager.merge(trainer);
        } else {
            entityManager.persist(trainer);
        }
        
        entityManager.getTransaction().commit();
        
        return trainer;
    }
    
    public void deleteById(Long id) {
        entityManager.getTransaction().begin();
        entityManager.remove(this.findById(id));
        entityManager.getTransaction().commit();
    }
}
