package pl.lsea.skijumping.ejbs;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateful;

@Stateful
public class SkijumpersList implements SkijumpersListRemote {

    List<String> values = new ArrayList<>();
    
    @Override
    public void addSkiJumper(String skiJumper) {
        this.values.add(skiJumper);
    }

    @Override
    public void removeSkiJumper(String skiJumper) {
        this.values.remove(skiJumper);
    }

    @Override
    public List<String> getSkiJumpers() {
        return this.values;
    }
    
}
