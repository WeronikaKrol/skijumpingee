package pl.lsea.skijumping.ejbs;


import java.util.List;
import javax.ejb.Remote;

@Remote
public interface SkijumpersListRemote {
    
    void addSkiJumper(String skiJumper);
    void removeSkiJumper(String skiJumper);
    
    List<String> getSkiJumpers();
}