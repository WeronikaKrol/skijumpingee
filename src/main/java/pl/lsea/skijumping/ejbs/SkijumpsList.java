package pl.lsea.skijumping.ejbs;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateful;

@Stateful
public class SkijumpsList implements SkijumpsListRemote {

    List<String> values = new ArrayList<>();
    
    @Override
    public void addSkiJump(String skiJump) {
        this.values.add(skiJump);
    }

    @Override
    public void removeSkiJump(String skiJump) {
        this.values.remove(skiJump);
    }

    @Override
    public List<String> getSkiJumps() {
        return this.values;
    }
    
}
