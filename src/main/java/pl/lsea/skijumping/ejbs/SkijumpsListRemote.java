package pl.lsea.skijumping.ejbs;


import java.util.List;
import javax.ejb.Remote;

@Remote
public interface SkijumpsListRemote {
    
    void addSkiJump(String skiJump);
    void removeSkiJump(String skiJump);
    
    List<String> getSkiJumps();
}