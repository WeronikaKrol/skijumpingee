package pl.lsea.skijumping.entity;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Temporal;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;
import pl.lsea.skijumping.util.Country;

/**
 * Abstract class
 */
@Entity
@SuperBuilder(toBuilder = true)
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@ToString
@Inheritance(strategy=InheritanceType.TABLE_PER_CLASS)
public abstract class Human implements Serializable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
    /**
     * Example of protected access to field
     */
    @NotNull
    @Size(min = 1, max = 255)
    protected String firstname;
    
    @NotNull
    @Size(min = 1, max = 255)
    protected String lastname;
    
    @NotNull
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    protected Date birthdate;
    
    /**
     * Field using enum "Country"
     */
    @NotNull
    @Enumerated(EnumType.STRING)
    protected Country country;
    
    public String getFormattedBirthdate() {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        
        return format.format(this.birthdate);
    }
    
}
