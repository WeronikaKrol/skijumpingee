package pl.lsea.skijumping.entity;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import pl.lsea.skijumping.util.Country;

@Entity
@Table(name = "SKI_JUMP")
@Setter
@Getter
@Builder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@ToString
public class SkiJump implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Size(min = 1, max = 255)
    private String name;

    @Size(max = 500)
    private String description;
    
    /**
     * Field using enum "Country"
     */
    @NotNull
    @Enumerated(EnumType.STRING)
    private Country country;
    
    @NotNull
    @Min(1)
    @Max(350)
    private Double constructionPoint;

    @Min(1)
    @Max(350)
    private Double worldRecord;
    
    @ManyToOne
    private SkiJumper recordist;
    
}
