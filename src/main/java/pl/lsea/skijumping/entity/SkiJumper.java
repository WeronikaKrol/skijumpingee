package pl.lsea.skijumping.entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;
import javax.validation.constraints.NotNull;

/**
 * Class that extends abstract class
 */
@Entity
@Table(name = "SKI_JUMPER")
@Setter
@Getter
@SuperBuilder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@ToString
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
public class SkiJumper extends Human implements Serializable {

    @NotNull
    @Min(1)
    @Max(250)
    private Double height;
    
    @NotNull
    @Min(1)
    @Max(200)
    private Double weight;
    
    @NotNull
    @Min(1)
    @Max(350)
    private Double privateRecord;
    
    @ManyToOne
    private Trainer trainer;
    
    @OneToMany(mappedBy = "team")
    private List<TeamSkiJumper> teams;
    
}
