package pl.lsea.skijumping.entity;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * !! Many-to-Many relationship !! - connects
 * Team with SkiJumpers via TeamSkiJumper table
 */
@Entity
@Table(name = "TEAM_SKI_JUMPER")
@Setter
@Getter
@Builder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@ToString
public class TeamSkiJumper implements Serializable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
    @ManyToOne
    @JoinColumn(name = "ski_jumper_id")
    private SkiJumper skiJumper;
    
    @ManyToOne
    @JoinColumn(name = "team_id")
    private Team team;
}
