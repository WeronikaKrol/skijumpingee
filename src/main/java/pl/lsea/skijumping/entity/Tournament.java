package pl.lsea.skijumping.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@ToString
@Inheritance(strategy=InheritanceType.TABLE_PER_CLASS)
public abstract class Tournament implements Serializable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
    protected String name;
    
    protected String description;
    
    @ManyToOne
    protected SkiJump skiJump;
    
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    protected Date startedAt;
    
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    protected Date endedAt;
}
