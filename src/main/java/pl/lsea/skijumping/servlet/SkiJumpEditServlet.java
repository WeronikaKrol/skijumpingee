package pl.lsea.skijumping.servlet;

import java.io.IOException;
import java.util.Set;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import pl.lsea.skijumping.dao.SkiJumpDao;
import pl.lsea.skijumping.dao.SkiJumperDao;
import pl.lsea.skijumping.entity.SkiJump;
import pl.lsea.skijumping.util.Country;

@WebServlet("/skijump/edit")
public class SkiJumpEditServlet extends HttpServlet {

    private SkiJumpDao dao;

    private SkiJumperDao udao;

    @Override
    public void init() throws ServletException {
        dao = new SkiJumpDao();
        udao = new SkiJumperDao();
    }

    @Override
    protected void doGet(
            HttpServletRequest req,
            HttpServletResponse resp
    ) throws ServletException, IOException {
        req.setAttribute("countries", Country.values());
        req.setAttribute("skiJumpers", udao.findAll());
        req.setAttribute("item", dao.findById(Long.parseLong(req.getParameter("id"))));

        RequestDispatcher requestDispatcher = req.getRequestDispatcher("../skijump.jsp");

        requestDispatcher.forward(req, resp);
    }

    @Override
    protected void doPost(
            HttpServletRequest req,
            HttpServletResponse resp
    ) throws ServletException, IOException {
        SkiJump skiJump = dao.findById(Long.parseLong(req.getParameter("id")));
        String constructionPoint = req.getParameter("constructionPoint");
        String worldRecord = req.getParameter("worldRecord");
        
        if(constructionPoint.isEmpty()) {
            constructionPoint = "0";
        }
        
        if(worldRecord.isEmpty()) {
            worldRecord = "0";
        }
        
        skiJump = skiJump
                .toBuilder()
                .name(req.getParameter("name"))
                .description(req.getParameter("description"))
                .country(Country.valueOf(req.getParameter("country")))
                .constructionPoint(Double.parseDouble(constructionPoint))
                .worldRecord(Double.parseDouble(worldRecord))
                .recordist(udao.findById(Long.parseLong(req.getParameter("recordist"))))
                .build();

        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
        
        Set<ConstraintViolation<SkiJump>> errors = validator.validate(skiJump);
        
        if(errors.size() > 0) {
            req.setAttribute("errors", errors);
            req.setAttribute("countries", Country.values());
            req.setAttribute("skiJumpers", udao.findAll());
            req.setAttribute("item", dao.findById(Long.parseLong(req.getParameter("id"))));
            
            RequestDispatcher requestDispatcher = req.getRequestDispatcher("../skijump.jsp");
            requestDispatcher.forward(req, resp);
        } else {
            dao.save(skiJump);

            resp.sendRedirect(req.getContextPath() + "/skijumps");
        }
    }
}
