package pl.lsea.skijumping.servlet;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Set;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import pl.lsea.skijumping.dao.SkiJumperDao;
import pl.lsea.skijumping.dao.TrainerDao;
import pl.lsea.skijumping.entity.SkiJumper;
import pl.lsea.skijumping.util.Country;

@WebServlet("/skijumper/add")
public class SkiJumperAddServlet extends HttpServlet {
 
    private SkiJumperDao dao;
    
    private TrainerDao udao;

    @Override
    public void init() throws ServletException {
        dao = new SkiJumperDao();
        udao = new TrainerDao();
    }

    @Override
    protected void doGet(
            HttpServletRequest req,
            HttpServletResponse resp
    ) throws ServletException, IOException {
        req.setAttribute("countries", Country.values());
        req.setAttribute("trainers", udao.findAll());
        
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("../skijumper.jsp");

        requestDispatcher.forward(req, resp);
    }
    
    @Override
    protected void doPost(
            HttpServletRequest req,
            HttpServletResponse resp
    ) throws ServletException, IOException {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date birthdate;
        try {
            birthdate = format.parse(req.getParameter("birthdate"));
        } catch (ParseException ex) {
            birthdate = new Date();
        }
        
        String height = req.getParameter("height");
        String weight = req.getParameter("weight");
        String privateRecord = req.getParameter("privateRecord");
        String trainerId = req.getParameter("trainer");
        
        if(height.isEmpty()) {
            height = "0";
        }
        
        if(weight.isEmpty()) {
            weight = "0";
        }
        
        if(privateRecord.isEmpty()) {
            privateRecord = "0";
        }
        
        if(trainerId.isEmpty()) {
            trainerId = "0";
        }
        
        SkiJumper skiJumper = SkiJumper
                            .builder()
                            .firstname(req.getParameter("firstname"))
                            .lastname(req.getParameter("lastname"))
                            .birthdate(birthdate)
                            .country(Country.valueOf(req.getParameter("country")))
                            .height(Double.parseDouble(height))
                            .weight(Double.parseDouble(weight))
                            .privateRecord(Double.parseDouble(privateRecord))
                            .trainer(udao.findById(Long.parseLong(trainerId)))
                            .build();
        
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
        
        Set<ConstraintViolation<SkiJumper>> errors = validator.validate(skiJumper);
        
        if(errors.size() > 0) {
            req.setAttribute("errors", errors);
            req.setAttribute("countries", Country.values());
            req.setAttribute("trainers", udao.findAll());
            
            RequestDispatcher requestDispatcher = req.getRequestDispatcher("../skijumper.jsp");
            requestDispatcher.forward(req, resp);
        } else {
            dao.save(skiJumper);
        
            resp.sendRedirect(req.getContextPath() + "/skijumpers");
        }
    }  
}
