package pl.lsea.skijumping.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import pl.lsea.skijumping.dao.SkiJumperDao;

@WebServlet("/skijumper/delete")
public class SkiJumperDeleteServlet extends HttpServlet {
    
    private SkiJumperDao dao;

    @Override
    public void init() throws ServletException {
        dao = new SkiJumperDao();
    }

    @Override
    protected void doPost(
            HttpServletRequest req,
            HttpServletResponse resp
    ) throws ServletException, IOException {
        Long id = Long.parseLong(req.getParameter("id"));
        dao.deleteById(id);
        
        resp.sendRedirect(req.getContextPath() + "/skijumpers");
    }
}
