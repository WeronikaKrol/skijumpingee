package pl.lsea.skijumping.servlet;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import pl.lsea.skijumping.dao.SkiJumpDao;

@WebServlet("/skijumps")
public class SkiJumpsServlet extends HttpServlet {

    private SkiJumpDao dao;

    @Override
    public void init() throws ServletException {
        dao = new SkiJumpDao();
    }
    
    @Override
    protected void doGet(
            HttpServletRequest req,
            HttpServletResponse resp
    ) throws ServletException, IOException {        
        req.setAttribute("items", dao.findAll());
        
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("skijumps.jsp");

        requestDispatcher.forward(req, resp);
    } 
}
