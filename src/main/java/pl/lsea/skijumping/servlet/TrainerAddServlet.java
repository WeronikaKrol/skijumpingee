package pl.lsea.skijumping.servlet;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Set;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import pl.lsea.skijumping.dao.TrainerDao;
import pl.lsea.skijumping.entity.Trainer;
import pl.lsea.skijumping.util.Country;

@WebServlet("/trainer/add")
public class TrainerAddServlet extends HttpServlet {
 
    private TrainerDao dao;

    @Override
    public void init() throws ServletException {
        dao = new TrainerDao();
    }

    @Override
    protected void doGet(
            HttpServletRequest req,
            HttpServletResponse resp
    ) throws ServletException, IOException {
        req.setAttribute("countries", Country.values());
        
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("../trainer.jsp");

        requestDispatcher.forward(req, resp);
    }
    
    @Override
    protected void doPost(
            HttpServletRequest req,
            HttpServletResponse resp
    ) throws ServletException, IOException {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date birthdate;
        try {
            birthdate = format.parse(req.getParameter("birthdate"));
        } catch (ParseException ex) {
            birthdate = new Date();
        }
        
        Trainer trainer = Trainer
                            .builder()
                            .firstname(req.getParameter("firstname"))
                            .lastname(req.getParameter("lastname"))
                            .birthdate(birthdate)
                            .country(Country.valueOf(req.getParameter("country")))
                            .representation(req.getParameter("representation"))
                            .build();
        
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
        
        Set<ConstraintViolation<Trainer>> errors = validator.validate(trainer);
        
        if(errors.size() > 0) {
            req.setAttribute("errors", errors);
            req.setAttribute("countries", Country.values());
            
            RequestDispatcher requestDispatcher = req.getRequestDispatcher("../trainer.jsp");
            requestDispatcher.forward(req, resp);
        } else {
            dao.save(trainer);
        
            resp.sendRedirect(req.getContextPath() + "/trainers");
        }
    }
}
