package pl.lsea.skijumping.servlet;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import pl.lsea.skijumping.dao.TrainerDao;

@WebServlet("/trainers")
public class TrainersServlet extends HttpServlet {
    
    private TrainerDao dao;

    @Override
    public void init() throws ServletException {
        dao = new TrainerDao();
    }
    
    @Override
    protected void doGet(
            HttpServletRequest req,
            HttpServletResponse resp
    ) throws ServletException, IOException {        
        req.setAttribute("items", dao.findAll());
        
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("trainers.jsp");

        requestDispatcher.forward(req, resp);
    }
    
    @Override
    protected void doPost(
            HttpServletRequest req,
            HttpServletResponse resp
    ) throws ServletException, IOException {
        
    }
    
    @Override
    protected void doDelete(
            HttpServletRequest req,
            HttpServletResponse resp
    ) throws ServletException, IOException {
    
    } 
}
