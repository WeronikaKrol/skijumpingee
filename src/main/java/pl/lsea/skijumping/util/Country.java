package pl.lsea.skijumping.util;

/**
 * Enum type
 */
public enum Country {
    POLAND,
    NORWAY,
    SLOVENIA,
    GERMANY,
    AUSTRIA,
    RUSSIA,
    USA,
    CZECH_REPUBLIC,
    JAPAN,
    CANADA,
    FINLAND
}
