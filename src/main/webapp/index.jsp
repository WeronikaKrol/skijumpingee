<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
        <title>LSEA 11</title>
    </head>
    <body>
        <div class='container'>
            <h1>Hello World!</h1>
            
            <ul>
                <li><a href='/togetheree/skijumpers'>SkiJumpers</a></li>
                <li><a href='/togetheree/skijumps'>SkiJumps</a></li>
                <li><a href='/togetheree/trainers'>Trainers</a></li>
                <li><a href='/togetheree/skijumps-list.jsp'>SkiJumps List - EJB Session</a></li>
                <li><a href='/togetheree/skijumpers-list.jsp'>SkiJumpers List - EJB Session</a></li>
            </ul>
        </div>
        
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>
    </body>
</html>
