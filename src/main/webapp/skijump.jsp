<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
        <title>LSEA 10</title>
    </head>
    <body>
        <div class='container'>
            <h1>${item != null ? "Edit" : "Add"} SkiJump ${item.id}</h1>
            
            <form method='POST'>
                <div class="mb-3">
                    <label class='form-label' for='name'>Name</label>
                    <input
                        type='text'
                        name='name'
                        id='name'
                        class='form-control'
                        value='${item.name}'
                    />
                    <ul>
                        <c:forEach items="${errors}" var="error">
                            <c:if test="${error.getPropertyPath() == 'name'}">
                                <li>${error.getMessage()}</li>
                            </c:if>
                        </c:forEach>
                    </ul>
                </div>
                
                <div class="mb-3">
                    <label class='form-label' for='description'>Description</label>
                    <input
                        type='text'
                        name='description'
                        id='description'
                        class='form-control'
                        value='${item.description}'
                    />
                    <ul>
                        <c:forEach items="${errors}" var="error">
                            <c:if test="${error.getPropertyPath() == 'description'}">
                                <li>${error.getMessage()}</li>
                            </c:if>
                        </c:forEach>
                    </ul>
                </div>
                
                <div class="mb-3">
                    <label class='form-label' for='country'>Country</label>
                    <select
                        name='country'
                        id='country'
                        class='form-control'
                    >
                        <c:forEach items="${countries}" var="country">
                            <option ${country == item.country ? 'selected="selected"' : ''} value='${country}'>${country}</option>
                        </c:forEach>
                    </select>
                    <ul>
                        <c:forEach items="${errors}" var="error">
                            <c:if test="${error.getPropertyPath() == 'country'}">
                                <li>${error.getMessage()}</li>
                            </c:if>
                        </c:forEach>
                    </ul>
                </div>
                
                <div class="mb-3">
                    <label class='form-label' for='constructionPoint'>Construction point</label>
                    <input
                        type='number'
                        step='0.1'
                        name='constructionPoint'
                        id='constructionPoint'
                        class='form-control'
                    />
                    <ul>
                        <c:forEach items="${errors}" var="error">
                            <c:if test="${error.getPropertyPath() == 'constructionPoint'}">
                                <li>${error.getMessage()}</li>
                            </c:if>
                        </c:forEach>
                    </ul>
                </div>
                
                <div class="mb-3">
                    <label class='form-label' for='worldRecord'>World Record</label>
                    <input
                        type='number'
                        step='0.1'
                        name='worldRecord'
                        id='worldRecord'
                        class='form-control'
                        value='${item.worldRecord}'
                    />
                    <ul>
                        <c:forEach items="${errors}" var="error">
                            <c:if test="${error.getPropertyPath() == 'worldRecord'}">
                                <li>${error.getMessage()}</li>
                            </c:if>
                        </c:forEach>
                    </ul>
                </div>
                
                <div class="mb-3">
                    <label class='form-label' for='recordist'>Recordist</label>
                    <select
                        name='recordist'
                        id='recordist'
                        class='form-control'
                    >
                        <option value=''>None</option>
                        <c:forEach items="${skiJumpers}" var="skiJumper">
                            <option ${skiJumper.id == item.recordist.id ? 'selected="selected"' : ''} value='${skiJumper.id}'>${skiJumper.firstname} ${skiJumper.lastname}</option>
                        </c:forEach>
                    </select>
                    <ul>
                        <c:forEach items="${errors}" var="error">
                            <c:if test="${error.getPropertyPath() == 'recordist'}">
                                <li>${error.getMessage()}</li>
                            </c:if>
                        </c:forEach>
                    </ul>
                </div>
                
                <button type='submit' class='btn btn-primary'>Save</button>
            </form>
        </div>
        
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>
    </body>
</html>
