<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
        <title>LSEA 10</title>
    </head>
    <body>
        <div class='container'>
            <h1>${item != null ? "Edit" : "Add"} SkiJumper ${item.id}</h1>
            
            <form method='POST'>
                <div class="mb-3">
                    <label class='form-label' for='firstname'>First Name</label>
                    <input
                        type='text'
                        name='firstname'
                        id='firstname'
                        class='form-control'
                        value='${item.firstname}'
                    />
                    <ul>
                        <c:forEach items="${errors}" var="error">
                            <c:if test="${error.getPropertyPath() == 'firstname'}">
                                <li>${error.getMessage()}</li>
                            </c:if>
                        </c:forEach>
                    </ul>
                </div>
                
                <div class="mb-3">
                    <label class='form-label' for='lastname'>Last Name</label>
                    <input
                        type='text'
                        name='lastname'
                        id='lastname'
                        class='form-control'
                        value='${item.lastname}'
                    />
                    <ul>
                        <c:forEach items="${errors}" var="error">
                            <c:if test="${error.getPropertyPath() == 'lastname'}">
                                <li>${error.getMessage()}</li>
                            </c:if>
                        </c:forEach>
                    </ul>
                </div>
                
                <div class="mb-3">
                    <label class='form-label' for='country'>Country</label>
                    <select
                        name='country'
                        id='country'
                        class='form-control'
                    >
                        <c:forEach items="${countries}" var="country">
                            <option ${country == item.country ? 'selected="selected"' : ''} value='${country}'>${country}</option>
                        </c:forEach>
                    </select>
                    <ul>
                        <c:forEach items="${errors}" var="error">
                            <c:if test="${error.getPropertyPath() == 'country'}">
                                <li>${error.getMessage()}</li>
                            </c:if>
                        </c:forEach>
                    </ul>
                </div>
                
                <div class="mb-3">
                    <label class='form-label' for='birthdate'>Birthdate</label>
                    <input
                        type='date'
                        name='birthdate'
                        id='birthdate'
                        class='form-control'
                        value='${item.formattedBirthdate}'
                    />
                    <ul>
                        <c:forEach items="${errors}" var="error">
                            <c:if test="${error.getPropertyPath() == 'birthdate'}">
                                <li>${error.getMessage()}</li>
                            </c:if>
                        </c:forEach>
                    </ul>                    
                </div>
                
                <div class="mb-3">
                    <label class='form-label' for='height'>Height</label>
                    <input
                        type='number'
                        step='0.1'
                        name='height'
                        id='height'
                        class='form-control'
                        value='${item.height}'
                    />
                    <ul>
                        <c:forEach items="${errors}" var="error">
                            <c:if test="${error.getPropertyPath() == 'height'}">
                                <li>${error.getMessage()}</li>
                            </c:if>
                        </c:forEach>
                    </ul>                    
                </div>
                
                <div class="mb-3">
                    <label class='form-label' for='weight'>Weight</label>
                    <input
                        type='number'
                        step='0.1'
                        name='weight'
                        id='weight'
                        class='form-control'
                        value='${item.weight}'
                    />
                    <ul>
                        <c:forEach items="${errors}" var="error">
                            <c:if test="${error.getPropertyPath() == 'weight'}">
                                <li>${error.getMessage()}</li>
                            </c:if>
                        </c:forEach>
                    </ul>
                </div>
                
                <div class="mb-3">
                    <label class='form-label' for='privateRecord'>Private Record</label>
                    <input
                        type='number'
                        step='0.1'
                        name='privateRecord'
                        id='privateRecord'
                        class='form-control'
                        value='${item.privateRecord}'
                    />
                    <ul>
                        <c:forEach items="${errors}" var="error">
                            <c:if test="${error.getPropertyPath() == 'privateRecord'}">
                                <li>${error.getMessage()}</li>
                            </c:if>
                        </c:forEach>
                    </ul>
                </div>
                
                <div class="mb-3">
                    <label class='form-label' for='trainer'>Trainer</label>
                    <select
                        name='trainer'
                        id='trainer'
                        class='form-control'
                    >
                        <option value=''>None</option>
                        <c:forEach items="${trainers}" var="trainer">
                            <option ${trainer.id == item.trainer.id ? 'selected="selected"' : ''} value='${trainer.id}'>${trainer.firstname} ${trainer.lastname}</option>
                        </c:forEach>
                    </select>
                    <ul>
                        <c:forEach items="${errors}" var="error">
                            <c:if test="${error.getPropertyPath() == 'trainer'}">
                                <li>${error.getMessage()}</li>
                            </c:if>
                        </c:forEach>
                    </ul>
                </div>
                
                <button type='submit' class='btn btn-primary'>Save</button>
            </form>
        </div>
        
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>
    </body>
</html>
