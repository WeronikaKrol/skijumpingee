<%@page import="javax.naming.InitialContext"%>
<%@page import="pl.lsea.skijumping.ejbs.SkijumpersListRemote"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<%!
    private static SkijumpersListRemote remote;

    public void jspInit() {
        try {
            InitialContext ic = new InitialContext();
            remote = (SkijumpersListRemote) ic.lookup("java:global/togetheree/SkijumpersList");
        } catch(Exception e) {
            System.out.println(e);
        }
    }
%>

<%
    if(request != null && request.getParameter("action") != null) {
        if(request.getParameter("action").equals("add")) {
            remote.addSkiJumper(request.getParameter("name"));
        } else if(request.getParameter("action").equals("delete")) {
            remote.removeSkiJumper(request.getParameter("name"));
        }
    }   
%>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
        <title>LSEA 11</title>
    </head>
    <body>
        <div class='container'>
            <div class='row'>
               <h1>SkiJumpers</h1> 
               <form method="POST">
                    <input type="hidden" name="action" value="add" />
                    <label class='form-label' for='name'>Name</label>
                    <input
                        type='text'
                        name='name'
                        id='name'
                        class='form-control'
                    />
                   <button class='btn btn-success'>Add</button>
               </form>
            </div>
            
            <table class='table'>
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                
                <tbody>
                    <%
                        for(String item : remote.getSkiJumpers()) {
                            out.println("<tr>"
                                + "<th scope='row'>" + item + "</th>"
                                + "<td>"                         
                                    + "<form class='m-1' method='POST'>"
                                        + "<input type='hidden' name='action' value='delete' />"
                                        + "<input type='hidden' name='name' value='" + item + "' />"
                                        + "<button class='btn btn-danger'>Delete</button>"
                                    + "</form>"                                
                                + "</td>"
                            + "</tr>");
                        }
                    %>
                </tbody>
            </table>
        </div>
        
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>
    </body>
</html>
