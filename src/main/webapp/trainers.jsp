<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
        <title>LSEA 10</title>
    </head>
    <body>
        <div class='container'>
            <div class='row'>
               <h1>Trainers</h1> 
               <a href='trainer/add'>
                   <button class='btn btn-success'>Add</button>
               </a>
            </div> 
            
            <table class='table'>
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Country</th>
                        <th>Birthdate</th>
                        <th>Representation</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                
                <tbody>
                    <c:forEach items="${items}" var="item">
                       <tr>
                            <th scope='row'>${item.id}</th>
                            <td>${item.firstname}</td>
                            <td>${item.lastname}</td>
                            <td>${item.country}</td>
                            <td>${item.birthdate}</td>
                            <td>${item.representation}</td>
                            <td>
                                <a class='m-1' href='trainer/edit?id=${item.id}'>
                                    <button class='btn btn-primary'>Edit</button>
                                </a>
                                
                                <form class='m-1' action="trainer/delete" method="POST">
                                    <input type="hidden" name="id" value="${item.id}" />
                                    <button class='btn btn-danger'>Delete</button>
                                </form>
                            </td>
                        </tr>
                    </c:forEach>                    
                </tbody>
            </table>
        </div>
        
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>
    </body>
</html>
